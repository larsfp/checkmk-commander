User interface sketches
=======================

Using markdown with html to sketch possible UI ideas. (Colors doesn't show up in gitlab, view in something else)

Current
-------

<pre>
<span style="color:lime;background:gray;">check-commander v0.8.11 | Updated 15:29:05 </span><span style="color:black;background:white;"> Type ? for help | Down: 0 Crit: 0 Warn: 0</span>

...data...

<span style="color:lime;background:gray;">15:29:00 connected to sites: test, </span>
</pre>

HTOP like tabs
--------------

<pre>
<span style="color:white;background:gray;">check-commander v0.8.11 | ← → to switch tabs | Type ? for help | 15:29:05</span>
Alerts <span style="color:white;background:gray;">Events </span> About

...data...

<span style="color:white;background:gray;">15:29:00 connected to sites: test, </span>
</pre>

Keep it to one line at top and bottom
-------------------------------------

<pre>
<span style="color:white;background:gray;">check-commander → Home | Type ? for help | 15:29:05</span>

<span style="color:white;background:gray;">check-commander → Events | Type ? for help | 15:29:05</span>

<span style="color:white;background:gray;">check-commander → About | Type ? for help | 15:29:05</span>
</pre>
